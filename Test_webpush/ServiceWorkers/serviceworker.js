﻿self.addEventListener('push', function (event) {
    if (event.data) {
        var msg_data = event.data.json();
        console.log(msg_data.body);
        console.log(msg_data.title);
        console.log(msg_data.icon);
    }
    event.waitUntil
        (self.registration.showNotification(msg_data.title,
            {
            body: msg_data.body,
            icon: msg_data.icon,
            image: msg_data.image
            }
            
        )
    );
});