﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using WebPush; // компонент для отправки запроса на уведомление на сервер

namespace WebPushApi
{
    // представление подписки пользователя. в таком формате фронтэнд присылает данные
    public class PushSubscription
    {
        public PushSubscription() { }
        public PushSubscription(string _endpoint, string _p256dh, string _auth)
        {
            this.Endpoint = _endpoint;
            this.Keys= new Dictionary<string, string>();
            this.Keys["p256dh"] = _p256dh;
            this.Keys["auth"] = _auth;
        }
        public string Endpoint { get; set; }           
        public IDictionary<string, string> Keys { get; set; }  // used for form binding in http request

        public string p256dh
        {
            set { this.Keys["p256dh"] = value; }
            get { return this.Keys["p256dh"]; }            
        }                
        public string auth
        {
            set { this.Keys["auth"] = value; }
            get { return this.Keys["auth"]; }
        }        
    }

    // сообщение для отправки через Push Notification
    public class PushMessage
    {
        public string body { get; set; }  // notification text
        public string title { get; set; } // notification title
        public string icon { get; set; } // URL to icon
        public string image { get; set; } // URL to big image

        public PushMessage(string _title,string _body)
        {
            this.body = _body;
            this.title = _title;
            this.icon = null;
            this.image = null;
        }

        public PushMessage(string _title, string _body,string _icon,string _image)
        {
            this.body = _body;
            this.title = _title;
            this.icon = _icon;
            this.image = _image;
        }

        //string with message data for sending to serviceworker script
        public string Payload ()
           {
            var msg_params = new Dictionary<string, string>();
            msg_params["body"] = this.body;
            msg_params["title"] = this.title;

            if (icon != null)
            { msg_params["icon"] = icon; }

            if (image != null)
            { msg_params["image"] = image; }

            string payload = JsonConvert.SerializeObject(msg_params);

            return payload;
            }
            
    }

    public class PushSender
    {
        static string publicServerKey = @"BMjIW6cW8AWs7zSTXiMjzwoCz5xu1Mke4qIL7pZEx9qQOsa3b3AEqN_YWNmSFaID0J0nlF8Ljq9ESoLTyLP5X1Q";
        static string privateServerKey = @"OCmT68H_oL9eZMojyhgLk24O8JWDJnxvkIVaQAEJkwE";
        static string subject = @"mailto:examplee@xample.com";

        public static bool SendPushMessage(WebPushApi.PushSubscription subs,PushMessage msg)
        {

            // coverting subscription to WebPush module format
            var subscription = new WebPush.PushSubscription(subs.Endpoint, subs.p256dh, subs.auth); 

            var vapidDetails = new VapidDetails(subject, PushSender.publicServerKey, PushSender.privateServerKey);
                       
            var webPushClient = new WebPushClient();
            try
            {
                webPushClient.SendNotification(subscription, msg.Payload(), vapidDetails);
            }
            catch (WebPushException exception)
            {
                Console.WriteLine("Http STATUS code" + exception.StatusCode);
                return false;
            }
            
            return true;
        }
   }
}
