﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;

using WebPush; // компонент для отправки запроса на уведомление на сервер
using Test_webpush.Models;
using WebPushApi;

namespace Test_webpush.Controllers
{

    public class HomeController : Controller
    {      
        private const string publicKey = @"BMjIW6cW8AWs7zSTXiMjzwoCz5xu1Mke4qIL7pZEx9qQOsa3b3AEqN_YWNmSFaID0J0nlF8Ljq9ESoLTyLP5X1Q";
        private const string privateKey = @"OCmT68H_oL9eZMojyhgLk24O8JWDJnxvkIVaQAEJkwE";
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Admin()
        {
            var context = new ModelPushNotification();

            return View(context);
        }

        public ActionResult Send(int id)
        {
            using (var context = new ModelPushNotification())
            {
                var s = context.Subscription.First(us => us.Id == id);
                var result = SendSinglePush(s.endpoint, s.p256dh, s.auth);
                return View(s);
            }                
           
        }




        [HttpPost]
        // push notification to single user by his subsription ID
        public JsonResult SendSingle(int id)
        {

            using (var context = new ModelPushNotification())
            {
                var sub = from cs in context.Subscription
                          where cs.Id == id
                          select cs;

                try
                {
                    foreach (var s in sub)
                    {
                        SendSinglePush(s.endpoint, s.p256dh, s.auth);
                    }
                }
                catch (Exception ex)
                { return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet); }
            }


            return Json(new { success = true });
        }

        [HttpPost]
        //check if user subscription to push notification is new and then save in DB
        public JsonResult Save(WebPushApi.PushSubscription subs)        
        {            
            int new_id;
            using (var context = new ModelPushNotification())
            {
                try
                {
                    int subscriptions_count = context.Subscription.Count(us => us.endpoint == subs.Endpoint);
                    
                    if (subscriptions_count == 0)
                    {
                        var new_subscription = new UserPushSubscription();

                        new_subscription.endpoint = subs.Endpoint;
                        new_subscription.p256dh = subs.Keys["p256dh"];
                        new_subscription.auth = subs.Keys["auth"];
                        new_subscription.type = "desktop"; // пока работаем только с браузерами на компах

                        context.Subscription.Add(new_subscription);
                        context.SaveChanges(); // переделать всё под асинхронный режим
                        new_id = new_subscription.Id;
                    }
                    else
                    {
                        new_id = context.Subscription.First(us => us.endpoint == subs.Endpoint).Id;
                    }
                }
                catch (Exception ex)
                { return Json(new { success = false,msg=ex.Message}, JsonRequestBehavior.AllowGet); }
            }
            
            return Json(new { success = true, new_id });
        }

        //send notification data to HUB
        private bool SendSinglePush(string endpoint_param, string p256dh_param, string auth_param)
        {
                                  
            // создаем подписку или берём её из внешнего источника
            var subscription = new WebPushApi.PushSubscription(endpoint_param, p256dh_param, auth_param);
            

            // create content and properties of push notification message
            var msg_body = "Тестовое сообщение с иконкой";
            var msg_title = "Заголовок тестового сообщения1";
            var msg_icon = "/Content/icon1.png";
            var msg_image = "/Content/image1_small.png";

            var msg = new WebPushApi.PushMessage(msg_title, msg_body, msg_icon, msg_image);

            return WebPushApi.PushSender.SendPushMessage(subscription, msg);

                        
        }
    }
}