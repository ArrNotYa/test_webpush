﻿// ОТКРЫТЫЙ КЛЮЧ приложения. Совпадает с private const string publicKey  в HomeController
APP_KEY = "BMjIW6cW8AWs7zSTXiMjzwoCz5xu1Mke4qIL7pZEx9qQOsa3b3AEqN_YWNmSFaID0J0nlF8Ljq9ESoLTyLP5X1Q";

// проверка возможностей браузера ------------------------
if (!('serviceWorker' in navigator)) {
    console.log("Service Worker isn't supported on this browser, disable or hide UI.");
    // Service Worker isn't supported on this browser, disable or hide UI.

}

else if (!('PushManager' in window)) {
    // Push isn't supported on this browser, disable or hide UI.
    console.log("Push isn't supported on this browser, disable or hide UI.");

}
else {
    console.log("Push supported");
}


navigator.serviceWorker.register('/scripts/serviceworker.js');

navigator.serviceWorker.ready
    .then(function (registration) {
        // Use the PushManager to get the user's subscription to the push service.
        console.log('service worker registered');
        return registration.pushManager.getSubscription()
            .then(async function (subscription) {
                // If a subscription was found, return it.
                if (subscription) {
                    console.log('Already subscribed', subscription.endpoint);
                    return subscription;
                }
                else { console.log('Not subscribed');}

                // Get the server's public key

                
                // Chrome doesn't accept the base64-encoded (string) vapidPublicKey yet
                // urlBase64ToUint8Array() is defined in /tools.js
                const convertedVapidKey = urlBase64ToUint8Array(APP_KEY);

                // Otherwise, subscribe the user (userVisibleOnly allows to specify that we don't plan to
                // send notifications that don't have a visible effect for the user).
                return registration.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: convertedVapidKey
                });
            });
    }).then(function (subscription) {
        // Send the subscription details to the server using the Fetch API.
        fetch('/Save', {
            method: 'post',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                subscription: subscription
            }),
        });
    });