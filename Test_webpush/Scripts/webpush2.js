﻿// примеры
// https://justmarkup.com/articles/2017-02-07-implementing-push-notifications/

// ОТКРЫТЫЙ КЛЮЧ приложения. Совпадает с private const string publicKey  в HomeController
const APP_KEY = "BMjIW6cW8AWs7zSTXiMjzwoCz5xu1Mke4qIL7pZEx9qQOsa3b3AEqN_YWNmSFaID0J0nlF8Ljq9ESoLTyLP5X1Q";

let serviceWorkerRegistration = null;


function registerServiceWorker() {
    console.log("startin registering Worker");    
    navigator.serviceWorker.register('/ServiceWorkers/serviceworker.js')
        .then(function (swr) {
            serviceWorkerRegistration = swr;
            console.log('Service worker successfully registered.1');
            subscribeUserToPush(); 
            //UnSubscribe();

        })  

        .catch(function (err) {
            console.error('Unable to register service worker.', err);
        });
}







function askPermission() {
    return new Promise(function (resolve, reject) {
        console.log("Ask for permission");
        const permissionResult = Notification.requestPermission(function (result) {
            resolve(result);
        });

        if (permissionResult) {
            permissionResult.then(resolve, reject); // !!!!!!!!!!!!!!!!!!!!!!!!! ПРОВЕРИТЬ, возможно, лишний кусок с промисом
        }
    })
        .then(function (permissionResult) {
            if (permissionResult !== 'granted') {
                throw new Error('We weren\'t granted permission.');
            }
            else { console.log("Permission granted"); }
        });
}


/*function UnSubscribe()
{
    console.log('current subsription unsubsribe');
    serviceWorkerRegistration.pushManager.getSubscription()
    .then(function (subscription) {
        if (subscription == null)
        {
            console.log("No subscriptions with serviceWorkerRegistration ");
            return;
        }
        subscription.unsubscribe()
        .then(function (successful)
            { console.log("You've successfully unsubscribed"); })
        .catch(function (e)
            {console.log(" Unsubscription failed");})
    })

}*/

function UnRegisterServiceWorkers() {
    navigator.serviceWorker.getRegistrations()
        .then(function (registrations) {
            for (let registration of registrations) {
                registration.unregister();
            }

        });
}


function subscribeUserToPush() {
    console.log("startin subscribing user");
    const subscribeOptions =
    {
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(APP_KEY)
    };
    
    serviceWorkerRegistration.pushManager.subscribe(subscribeOptions)
    .then(function (pushSubscription) {
        console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
        var bod = JSON.stringify(pushSubscription);
        return bod;
        })
        .then(function (pushSubscription) {
            console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
            
            console.log("saving subscription");
            fetch('/Home/Save', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: pushSubscription
            })
            .then(function (response) {                                    
                return (response.json());
                })
        .then(function (params) {
                console.log("Subscription data send to backend");
                console.log(JSON.stringify(params));
                $("#btnSubscribe").val(params.new_id);
                $("#btnSubscribe").toggle('slow');                
                $("#btnSubscribe").click(function () {
                    fetch('/Home/Send', {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ id: $("#btnSubscribe").val() })
                    })

                })

            })
        })

    .catch(function (err) {
            console.log('Failed to subscribe the user: ', err);
        });
}






this.window.onload = function () {
    // проверка возможностей браузера ------------------------
    if (!('serviceWorker' in navigator)) {
        console.log("Service Worker isn't supported on this browser, disable or hide UI.");
        // Service Worker isn't supported on this browser, disable or hide UI.
        return;
    }

    if (!('PushManager' in window)) {
        // Push isn't supported on this browser, disable or hide UI.
        console.log("Push isn't supported on this browser, disable or hide UI.");
        return;
    }
    console.log("Push supported");

    // регистрация service worker ---------------------------    
    askPermission();
    registerServiceWorker();
    
    
    // subscribeUserToPush(); 

    // запрос разрешения на подписку ------------------------
    

    // подписываем пользователя на рассылку наших сообщений
    // и отправляем данные подписки на сервер
    
}