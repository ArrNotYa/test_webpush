﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test_webpush.Models
{
    public class PushSubscriptionInputModel
    {
        public string Endpoint { get; set; }

        public IDictionary<string, string> Keys { get; set; }
    }
}