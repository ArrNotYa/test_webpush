namespace Test_webpush.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Subscription")]
    public partial class UserPushSubscription
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string endpoint { get; set; }

        [Required]
        [StringLength(150)]
        public string p256dh { get; set; }

        [Required]
        [StringLength(150)]
        public string auth { get; set; }

        [StringLength(10)]
        public string type { get; set; }

        [StringLength(30)]
        public string browser { get; set; }
    }
}
