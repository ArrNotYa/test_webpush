namespace Test_webpush.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelPushNotification : DbContext
    {
        public ModelPushNotification()
            : base("name=ModelPushNotification")
        {
        }

        public virtual DbSet<UserPushSubscription> Subscription { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserPushSubscription>()
                .Property(e => e.endpoint)
                .IsUnicode(false);

            modelBuilder.Entity<UserPushSubscription>()
                .Property(e => e.p256dh)
                .IsUnicode(false);

            modelBuilder.Entity<UserPushSubscription>()
                .Property(e => e.auth)
                .IsUnicode(false);

            modelBuilder.Entity<UserPushSubscription>()
                .Property(e => e.type)
                .IsFixedLength();

            modelBuilder.Entity<UserPushSubscription>()
                .Property(e => e.browser)
                .IsFixedLength();
        }
    }
}
